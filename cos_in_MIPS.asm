.data
	intro:		.asciiz "Enter a radian value: "
	finish1:	.asciiz "cos("
	finish2:	.asciiz ") = "

	userInput:
		.float 0.0
	resultZ:
		.float 0.0
	resultExp:
		.float 0.0
	resultFac:
		.float 0.0
	intSum:
		.float 0.0
	floatConstants:
		.float -1.0, 0.0, 1.0
	factArray:
		# 0!-24!
		# Generated with a C program to print these values
		.float 0.00, 1.00, 2.00, 6.00, 24.00, 120.00, 720.00, 5040.00, 40320.00, 362880.00, 3628800.00, 
			39916800.00, 479001600.0, 6227020800.0, 87178289152.0, 1307674279936.0, 20922788478976.0, 355687414628352.0, 6402373530419200.0,
			121645096004222980.0, 2432902023163674600.0, 51090940837169725000.0, 1124000724806013000000.0, 25852017444594486000000.0, 620448454699064670000000.0
		
.text
	main:
		li $v0, 4
		la $a0, intro
		syscall
	
		li $v0, 6		# read float
		syscall
	
		swc1 $f0, userInput	# floats read into $f0, store in userInput
		
		ori $a0, $0, 10		# Number of terms to calculate out to. Weird interaction beyond 6 terms.
		jal cosLooper
		
		
		li $v0, 4
		la $a0, finish1
		syscall
	
		li $v0, 2		# print float
		lwc1 $f12, userInput	# load userInput into print register
		syscall
	
		li $v0, 4
		la $a0, finish2
		syscall
	
		li $v0, 2		# print float
		#lwc1 $f12, resultZ	# load result into print register
		mtc1 $v1, $f12
		syscall
	
		li $v0, 10		# Terminate Program
		syscall
	
	############################################
	cosLooper:
		# $a0 contains number of terms to calculate to, returns $v1
		# Uses f0, f1, f2, t0, t1, $f12
		# cos(x) = (-1)^n * (x^2n)/(2n)!
		
		addi $sp, $sp, -36	# allocate stack space
		
		swc1 $f6, 32($sp)	# Intermediate Sum Variable replacement
		swc1 $f5, 28($sp)	# User Input Replacement
		swc1 $f12, 24($sp)	
		
		sw $t1, 20($sp)		# store $t1 on stack
		sw $t0, 16($sp)		# store $t0 on stack
		
		mfc1 $t0, $f0		# move $f0 into $t0
		sw $t0, 12($sp)		# store $f0 on stack
		
		mfc1 $t0, $f1
		sw $t0, 8($sp)		# store $f1 on stack
		
		mfc1 $t0, $f2
		sw $t0, 4($sp)		# store $f0 on stack
		
		sw $ra, 0($sp)		# store return address on stack
		
		la $t0, floatConstants
		
		lwc1 $f5, userInput
		lwc1 $f6, 4($t0)	# load 0.0 into $t6 for use as intermediate sum
		
	cosLoop:
		la $t1, factArray
		
		lwc1 $f0, 0($t0)	# load -1.0 into $f0
		addi $a1, $a0, 0	# set $a1 = $a0
		jal exponent		# calculate -1 ^ n stored in resultExt
		mov.s $f1, $f12		# load -1 ^ n into $f1
		
		lwc1 $f0, userInput	# load x into $f0
		mul $a1, $a0, 2		# $a1 = 2 * n
		jal exponent
		#lwc1 $f2, resultExp	# load x^(2n) into $f2
		mov.s $f2, $f12		# load x^(2n) into $f2
		
		mul $a1, $a0, 2
		mul $a1, $a1, 4		# convert 2n to a byte count
		add $t1, $t1, $a1	# add this byte count to the address of the array in ($t1)
		lwc1 $f3, 0($t1)	# access word at the address we just calculated for factorial denominator
		
		div.s $f2, $f2, $f3	# $f2 = x^(2n) / (2n)! 
		mul.s $f1, $f1, $f2	# multiply previous sum by (-1) ^ n, store in $f1
		
		#lwc1 $f2, intSum	# load the intermediate sum value
		#add.s $f1, $f1, $f2	# add newly calculated value to intermediate sum
		#swc1 $f1, intSum	# store new intermediate sum
		add.s $f6, $f6, $f1	# Add term value to intermediate sum
		
		addi $a0, $a0, -1
		bgt $a0 $0, cosLoop
		# break on just greater than equal as term n = 0 should == 1.0
		
	cosTerm:
		#lwc1 $f0, intSum	# load intermediate cos sum into $f0
		lwc1 $f2, 8($t0)	# load 1.0 into $f2
		
		#add.s $f0, $f2, $f0	# add 1.0 to intermediate sum aka. add n=0 == 1.0 to int sum
		#swc1 $f0, resultZ	# store cos sum approximation in resultZ
		
		add.s $f6, $f2, $f6	# Add 1.0 to intermediate sub
		mfc1 $v1, $f6		# move final result into return 
		
		lw $ra, 0($sp)		# restore return address
		
		lw $t0, 4($sp)		# restore original $f2
		mtc1 $t0, $f2
		
		lw $t0, 8($sp)		# restore original $f1
		mtc1 $t0, $f1
		
		lw $t0, 12($sp)		# restore original $f0
		mtc1 $t0, $f0
		
		lw $t0, 16($sp)		# restore original $t0
		lw $t1, 20($sp)		# restore original $t1
		
		lwc1 $f12, 24($sp)
		lwc1 $f5, 28($sp)
		lwc1 $f6, 32($sp)
		
		addi $sp, $sp, 36	# reset stack pointer
		jr $ra
	###########################################
	exponent:
	# uses t0, f30, f31
		# $a1 should contain the power integer, $f0 should contain argument
		# result returned in $f0 Stored into address of label resultZ
		addi $sp, $sp, -12
		sw $t0, 8($sp)		# original $t0 store on stack
		mfc1 $t0, $f30
		sw $t0, 4($sp)		# store f30 on stack
		mfc1 $t0, $f31
		sw $t0, 0($sp)		# store f31 on stack
		
		mov.s $f30, $f0 
		mov.s $f31, $f0
	interim:
		ble $a1, 0, zeroCase		# If the input power is 0 go to the 0 case where the function will return 1 
		bge $a1, 2, exponentLoop	# If the input power is 2 or greater divert to the exponentLoop label
		j terminate			# If the power is currently 1 terminate the function and return whatever value in $f30 and resultExp
	terminate:		
		#swc1 $f30, resultExp	# store result in memory
		mov.s $f12, $f30
		
		lw $t0, 0($sp)		# reload $f31
		mtc1 $t0, $f31
		lw $t0, 4($sp)		# reload $f30
		mtc1 $t0, $f30
		lw $t0, 8($sp)		# reload original $t0
		addi $sp, $sp, 12
		
		jr $ra
		
	zeroCase:			# if a1 = 0 return 1 as the output in f30
		la $t0, floatConstants
		lwc1 $f30, 8($t0)	# load 1.0 from floatConstants array
		j terminate
	
	exponentLoop:
		mul.s $f30, $f30, $f31
		addi $a1, $a1, -1
		j interim
		
	#########################################
