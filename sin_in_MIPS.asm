###########################################
#	Loop and Numerical Methods based sin(x) calculator
#	For use in larger project file, however supporting IO is provided such that
#		this file may be run as a standalone sin(x) calculator.
#	This only operates to single precision.

.data
	intro:		.asciiz "Enter a radian value: "
	finish1:	.asciiz "sin("
	finish2:	.asciiz ") = "

	userInput:
		.float 0.0
	resultZ:
		# Temp for use in this example program
		.float 0.0
	resultExp:
		# Can likely be replaced with register storage instead of storing and reading from DM
		.float 0.0
	resultFac:
		.float 0.0
	intSum:
		# This can likely be replaced with register storage instead of storing and reading from DM
		.float 0.0
	floatConstants:
		.float -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0
	factArray:
		#.float	0x00000000, 0x3F800000, 0x40000000, 0x40C00000, 0x41C00000, 0x42F00000, 0x44340000, 0x459D800, 0x471D800, 0x48B13000, 0x4A5D7C00, 0x4C184540
		# 0!-24!
		# Generated with a C program to print these values
		.float 0.00, 1.00, 2.00, 6.00, 24.00, 120.00, 720.00, 5040.00, 40320.00, 362880.00, 3628800.00, 
			39916800.00, 479001600.0, 6227020800.0, 87178289152.0, 1307674279936.0, 20922788478976.0, 355687414628352.0, 6402373530419200.0,
			121645096004222980.0, 2432902023163674600.0, 51090940837169725000.0, 1124000724806013000000.0, 25852017444594486000000.0, 620448454699064670000000.0
		
.text
	main:
		li $v0, 4
		la $a0, intro
		syscall
	
		li $v0, 6		# read float
		syscall
	
		swc1 $f0, userInput	# floats read into $f0, store in userInput
		
		ori $a0, $0, 10		# Number of terms to calculate out to. Weird interaction beyond 6 terms.
		lw $a1, userInput
		jal sinLooper
		
		
		li $v0, 4
		la $a0, finish1
		syscall
	
		li $v0, 2		# print float
		lwc1 $f12, userInput	# load userInput into print register
		syscall
	
		li $v0, 4
		la $a0, finish2
		syscall
	
		li $v0, 2		# print float
		mtc1 $v1, $f12		# load result into print register
		syscall
	
		li $v0, 10		# Terminate Program
		syscall
	
	############################################
	# SIN()
	sinLooper:
		# $a0 contains number of terms to calculate to, takes X in from $a1, returns in $v1
		# Uses f0, f1, f2, t0, t1, f12
		# sin(x) = (-1)^n * (x^2n+1)/(2n+1)!
		
		addi $sp, $sp, -36	# allocate stack space
		
		swc1 $f6, 32($sp)	# Intermediate Sum Variable replacement
		swc1 $f5, 28($sp)	# User Input Replacement
		swc1 $f12, 24($sp)	
		
		sw $t1, 20($sp)		# store $t1 on stack
		sw $t0, 16($sp)		# store $t0 on stack

		swc1 $f0, 12($sp)	# store $f0 on stack
		swc1 $f1, 8($sp)	# store $f1 on stack
		swc1 $f2, 4($sp)	# store $f2 on stack
		
		sw $ra, 0($sp)		# store return address on stack
		
		la $t0, floatConstants
		
		mtc1 $a1, $f5
		lwc1 $f6, 4($t0)	# load 0.0 into $t6 for use as intermediate sum
		
	sinLoop:
		la $t1, factArray
		
		lwc1 $f0, 0($t0)	# load -1.0 into $f0
		addi $a1, $a0, 0	# set $a1 = $a0
		jal exponent		# calculate -1 ^ n stored in resultExt
		mov.s $f1, $f12		# load -1 ^ n into #$f1
		
		#lwc1 $f0, userInput	# load x into $f0
		mov.s $f0, $f5		# load x into $f0
		mul $a1, $a0, 2		# $a1 = 2 * n
		addi $a1, $a1, 1	# $a1 = $a1 + 1 where $a1 initially equal to 2n thus $a1 = (2*n) + 1
		jal exponent
		mov.s $f2, $f12		# load x^(2n+1) into $f2
		
		mul $a1, $a0, 2
		addi $a1, $a1, 1	# a1 now = 2 * n + 1 again
		mul $a1, $a1, 4		# convert 2n+1 to a byte count
		add $t1, $t1, $a1	# add this byte count to the address of the array in ($t1)
		lwc1 $f3, 0($t1)	# access word at the address we just calculated for factorial denominator
		
		div.s $f2, $f2, $f3	# $f2 = x^(2n+1) / (2n + 1)! 
		mul.s $f1, $f1, $f2	# multiply previous sum by (-1) ^ n, store in $f1
		
		#lwc1 $f2, intSum	# load the intermediate sum value
		#add.s $f1, $f1, $f2	# add newly calculated value to intermediate sum
		#swc1 $f1, intSum	# store new intermediate sum
		
		add.s $f6, $f6, $f1	# add term value into $t6 intermediate sum
		
		addi $a0, $a0, -1
		
		bge $a0 $0, sinLoop
		
	sinTerm:
		#lwc1 $f0, intSum	# load intermediate sin sum into $f0
		#swc1 $f0, resultZ	# store sin sum approximation in resultZ
		
		#mov.s $f12, $f6
		#swc1 $f6, resultZ
		mfc1 $v1, $f6		# move result into $v1
		
		lw $ra, 0($sp)		# restore return address
		lwc1 $f2, 4($sp)	# restore $f2
		lwc1 $f1, 8($sp)	# restore $f1
		lwc1 $f0, 12($sp)	# restore $f0
		
		lw $t0, 16($sp)		# restore $t0
		lw $t1, 20($sp)		# restore $t1
		
		lwc1 $f12, 24($sp)
		lwc1 $f5, 28($sp)
		lwc1 $f6, 32($sp)
		
		addi $sp, $sp, 36	# reset stack pointer
		
		jr $ra
	
	###########################################
	##### EXP()
	exponent:
	# uses t0, f30, f31
		# $a1 should contain the power integer, $f0 should contain argument
		# result returned in $f0 Stored into address of label resultZ
		addi $sp, $sp, -12
		sw $t0, 8($sp)		# original $t0 store on stack
		swc1 $f30, 4($sp)	# store $f30 on stack
		swc1 $f31, 0($sp)	# store $f31 on stack
		
		mov.s $f30, $f0		# move x into $f30 and $f31 
		mov.s $f31, $f0		# x here being the base value
	
	interim:
		beq $a1, 0, zeroCase		# If the input power is 0 go to the 0 case where the function will return 1 
		bge $a1, 2, exponentLoop	# If the input power is 2 or greater divert to the exponentLoop label
	#	j terminate			# If the power is currently 1 terminate the function and return whatever value in $f30 and resultExp
	
	terminate:		
		# THIS SHOULD BE REPLACED BY ANOTHER $f REGISTER
		#swc1 $f30, resultExp	# store result in memory
		mov.s $f12, $f30	# store result in $f12

		lwc1 $f31, 0($sp)	# reload $f31
		lwc1 $f30, 4($sp)	# reload $f30
		lw $t0, 8($sp)		# reload $t0
		
		addi $sp, $sp, 12
		
		jr $ra
		
	zeroCase:			# if a1 = 0 return 1 as the output in f30
		la $t0, floatConstants
		lwc1 $f30, 8($t0)	# load 1.0 from floatConstants array
		j terminate
	
	exponentLoop:
		mul.s $f30, $f30, $f31	# multiply y * x where y is equal to every previous multiplication of x to x
		addi $a1, $a1, -1	# decrement the count: x ^ n -> n^(n-1)
		j interim		# jump to interim where the value of $a1 will determine the next operation
		
	#########################################
