# BACKUP IN bmpAnimTest2.asm
#######
#
# Bitmap Display Test

# This sketch is perfectly able to output standard 32-bit X8R8G8B8 format BMP files. Do NOT include color space information within the file.
#	If color space information is included no checks are provided and the image will be offset by a number of bytes.
#
# Also worth noting, compressed files are not accounted for in this version. Only raw BMP data exported from GIMP ( A8R8G8B8 or X8R8G8B8 ) settings may be used.
# For non symmetrical images a horizontal flip is required. Compute time is already long and flipping the array without even more space and time usage isn't nearly
#	as easy as preloading horizontally flipped images ( flipped across Y-axis )

.data

pxBuffer: 	.space	0x10000		# pixel H * pixel W * 4 bytes
fileBuffer:	.space	0x10054		# Buffer for file vals, image size + 54 bytes for BMP header, perfect size for 128x128 bmp
endianCorr:	.space	0x00200		# For use in a future version to flip the image horizontally (endian fully swapped) ( 128 words * 4 byes = 512 byte allocation = 0x200 )		

buffLen:	.word	0x10054
filter:		.word	0x00FFFFFF

#colP:		.word	512		# px * 4 for byte count
#rowP:		.word	512
#totB:		.word	0x40000		# Total byte count

#colW:		.word	128		# raw pixel data in word count
#rowW:		.word	128
#totW:		.word	0x04000		# sum total of words to search through, for some reason calling this label does nothing... so it's just here to provide a constant reference

bmpFile1:	.asciiz	"subSonarBitmap.bmp"
bmpFile2:	.asciiz	"test.bmp"
bmpFile3:	.asciiz	"test2.bmp"
bmpFile4:	.asciiz	"test3.bmp"
bmpFile5:	.asciiz	"testFrac.bmp"
bmpFile6:	.asciiz	"wave.bmp"
bmpFile7:	.asciiz	"sunset.bmp"



errorOnRead:	.asciiz	"Error on file read. Error Code: -1 (Failed Open)"
success:	.asciiz	"File opened sucessfully\n"
newL:		.asciiz "\n"

.text
	main:	
		la $a0, bmpFile1
		jal loadFileAndPrint
		#jal clearBuffer
		
		la $a0, bmpFile2
		jal loadFileAndPrint
		#jal clearBuffer
		
		la $a0, bmpFile4
		jal loadFileAndPrint
		#jal clearBuffer
		
		la $a0, bmpFile5
		jal loadFileAndPrint
		
		la $a0, bmpFile6
		jal loadFileAndPrint
		#jal clearBuffer
		
		la $a0, bmpFile7
		jal loadFileAndPrint
		
		li $v0, 10		# program terminate
		syscall
		
	######## Program Functions	
	loadFileAndPrint:
		# pre load $a0 with file path to call this function
		addi $sp, $sp, -4
		sw $ra, 0($sp)
		
		li $v0,	13		# read file
		li $a1,	0		# 0 flag
		li $a2, 0		# 0 flag
		syscall
		
		beq $v0, -1, openError	# if v0 == -1 for file error then print an error and terminate program
		
		move $t1, $v0		# if file opens successfully save file descriptor in $t1
		
		li $v0, 14		# read from file
		addi $a0, $t1, 0	# load file descriptor into $a0
		la $a1, fileBuffer	# output buffer for file read
		li $a2, 0x10054		# bytes to read ( px width * px height * 4 )
		syscall
		
		li $v0, 16		# close file
		move $a0, $t1		# load file descriptor into file, t1 can now be rewritten
		syscall
		
		jal printToScreen	# Print from file buffer onto bitmap
		
		lw $ra, 0($sp)
		addi $sp, $sp, 4
		jr $ra
		
	########################################
	printToScreen:			# Prints frame bytes from data in file buffer
		## Uses $t0 - $t6 and $s0 - $s3 and needs to store ra, allocate 48 bytes on stack
		addi $sp, $sp, -48
		sw $s3, 44($sp)
		sw $s2, 40($sp)
		sw $s1, 36($sp)
		sw $s0, 32($sp)
		sw $t6, 28($sp)
		sw $t5, 24($sp)
		sw $t4, 20($sp)
		sw $t3, 16($sp)
		sw $t2, 12($sp)
		sw $t1, 8($sp)
		sw $t0, 4($sp)
		sw $ra, 0($sp)
		
		la $t1, pxBuffer		# Base address for the frame buffer (to be printed by bitmap graphics tool)
		
		la $t2, fileBuffer		# Load base address for the file buffer into $t2 (contents of file are here)
		
		addi $t2, $t2, 0x46		# Add offset to account for BMP file header, effectively moving pointer beyond file header info. Loading everything as bytes is essential
						#	as this offset does not align with word boundaries.
		li $t3, 0x04000			# Load display size (row * col) into t3, in word count, this serves as the loop counter.
		lw $s0, filter			# Load 0x00FFFFFF as a filter to reject bits in order to form the 24 bit colorspace
	
		
		
						
	topL0:					# LOOP TO LOAD BYTES FROM FILE BUFFER INTO BYTE ARRAY. 
						# Operations: LOAD BYTE -> SHIFT BYTE -> COMBINE SHIFTED BYTES INTO A WORD -> REJECT BITS 24-32 -> CALCULATE OFFSET -> STORE WORD AT OFFSET
						# These are local variables that will initialize on every loop.
		li $t6, 4			# Loop Counter (int n = 4; n != 0; n--) where $t6 == n. Init = 4.
		li $t5, 0			# Temporary regiser where a byte will be read into, then shifted to the appropriate position. Init = 0.
		li $t7, 0			# The number of shifts to perform, variable. Init = 0.
		li $s1, 0			# Shift variable for loops, performs sub looping for shift counts based off of the value in $t7. Init = 0.
		li $s2, 0			# byte sum variable
		
		li $s3, 128			# constant for division
		
		# The bitshift logic ( topLin to skipShift ) is critically important to swap the endianness of the 32-bit color words stored in standard 32-bit .bmp files.
		# For example before this logic; a simple 4 byte read into a word ( say: 0x00555252 color code ) would represent in memory as ( 0x52525500 )
		#	With a simple variable shift of bytes (for loop) and temp register we can easily re-arrange bytes as we construct the word.
		# The process works as follows:
		#	Load a byte from fileArray pointer ( $t2 )
		#	Check loop counter ( initializes as 4 in $t6, $t6 = 3 at first pass ), shift the byte by loopCounter * 8 ( eg. the first read bit is shifted by 3 * 8 or 24 bits )
		#	Add this shifted byte into an buffer word ( $s2 )
		#	Perform this loop until $t6 == 0. 
		#		Shifting per byte read is as follows: 1st read << 24, 2nd read << 16, 3rd read << 8, 4th read << 0
		#	Logical AND this temporary word ( $s2 ) and a predefined filter 0x00FFFFFF ( $s0 )
		#	The word is now endian corrected and can be processed into the frame buffer.
		#
		# More details on register usage below:
		# Within the bitshift sub loop ( begins at label topLin ) there are 3 main working registers, $t5, $s1, and $s2.
		#	$s1 is a counter which is set at the start of the parent loop (on each word read from loop) and decrements.
		#		$s1 represents the number of times an 8 bit left shift should be performed. 
		#			This allows for N number of byte position shifts, where N is the value in $s1
		#
		#	$t5 is the temporary buffer which a byte is read into (from fileBuffer), then this byte undergoes the number of shifts prescribed by the value in $s1.
		#	$s2 is the word buffer (initializes to 0x00000000) where the appropriately shifted byte in $t5 is added. In order to clear the first 2 bits( X8 or A8 value )
		#		a logical AND is performed with the value in $s0 ( 0x00FFFFFF )
		#
		# Other used registers are:
		#	$t2: Array of bytes ( fileBuffer ) to read from. Incremented by 1 per loop cycle to read from the next byte in the buffer.
		#	$t7: Byte counter. This counts the number of bytes read and allows the bit shift sub loop counter ( $s1 ) to reference the value in order to perform adequate shifts.
		#	$t4: Redundant now.
		#	$t6: Decrement counter to ensure that ONLY 4 total bytes are loaded into the word.
		
		# BYTE LOAD -> SHIFT LOOP
		topLin:				# Sub loop which creates a usable word from multiple bytes. This loop also switches the endianness of the 24-bit color value.
						#	such that proper 0x00RRGGBB words are created
			addi $s1, $t7, 0	# Reset the loop sub variable to current count ($t7 is current counter)
			lb $t5, ($t2)
			
			addi $t2, $t2, 1	# Increment byte array read pointer by 1 byte
			addi $t6, $t6 -1	# Decrement loop counter by 1
			
			beqz $t6, eolLin	# if loop counter == 0 terminate sub loop (ie. we have loaded 4 bytes into a word, move on to word processing)
			
			# BYTE SHIFT LOOP
			shiftLabel:
				beqz $s1, skipShift	# skip bit shift if $s1 == 0, otherwise the loop will continue until s1 == 0, allowing variable length bitshifts of the byte	
				sll $t5, $t5, 8		# shift temp register by 8 bits
				addi $s1, $s1, -1	# decrement bit shift count
				j shiftLabel
			
		skipShift:			# COUNT UP TO 4 BITS, ADDi, and TEMP SUM
			addi $t7, $t7, 1	# Increment the shift counter (add another 8 bit shift to the next loaded byte)
			add $s2, $s2, $t5	# Add shifted byte to temp sum
			bnez $t6, topLin	# Jump to top of byte loading loop
	eolLin:
		and $s2, $s2, $s0		# Logical AND the temp register with 0x00FFFFFF to ensure the first 2 bits of display bytes are always 0x00
		
		# LOAD ROW REARRANGE
		# The next few lines calculates the appropriate pointer offset such that each row is loaded in the correct order ( word endianness correction )
		# We already have bytes that are in the correct orientation, but the bytes have been flipped in a relative sense preserving the order they are read in.
		# Without this correction the image will be flipped across one or more axis depending on the direction of array fill.
		#	Filling the frame buffer (pxBuffer) from the base address and incrementing leads to an image flip across y = x. 
		#	Filling the frame buffer (pxBuffer) from the final address and decrementing leads to an image flip across y = 0.
		#
		# The final function is as follows:
		#		loopCount % (modulo) 128
		#			Resultant = row number 		( 0 - 127 )
		#			Remainder = column number 	( 0 - 127 )
		#	Function representations may be interpreted as follows:
		#		( Offset Words ) = ( ( 128 * 4 ) - ( remainder * 4 ) ) + ( 128 * 4 * resultant)
		#		( Offset Words ) = ( 512 - ( remainder * 4 ) ) + ( 512 * resultant )
		#		( Offset Words ) = ( 512 - cols ) + ( 512 * rows ) ( IN BYTES )
		#		( Offset Words ) = ( maxCols - col ) + ( maxRows * row ) (psuedocode)
		#
		#	The purpose of this calculation is endian correction as the byte order read from bmp files results in a flipped bitmap.
		#	Effectively this is a reverse 2d array.
		#
		div $t3, $s3			# current counter value % 128
		mfhi $s4			# remainder of div
		mflo $s5			# resultant of div
		mul $s3, $s3, 4			# 128 * 4 = 512, word to byte conversion
		
		mul $s5, $s5, $s3		# resultant * 512
		mul $s4, $s4, 4			# remainder * 4
		sub $s3, $s3, $s4		# 512 - ( remainder * 4 )
		add $s3, $s3, $s5		# ( (512 - ( remainder * 4 )) + (512 * resultant) ) = number of words to offset $t1 (base address)
		
		add $s3, $t1, $s3		# offset pointer from base address of array
		
		sw $s2, ($s3)			# Store word the loop has created into the frame buffer array
		
		addi $t3, $t3, -1		# decrement loop
		
		bnez $t3, topL0			# jump to top of loop if $t3 =/= 0, this is the total word counter loop thingy
	
	## EOL. Words have all been created and stored by this point.
		
		lw $ra, 0($sp)
		lw $t0, 4($sp)
		lw $t1, 8($sp)
		lw $t2, 12($sp)
		lw $t3, 16($sp)
		lw $t4, 20($sp)
		lw $t5, 24($sp)
		lw $t6, 28($sp)
		lw $s0, 32($sp)
		lw $s1, 36($sp)
		lw $s2, 40($sp)
		lw $s3, 44($sp)
		addi $sp, $sp, 48
		jr $ra
#######
#######
	refresh:			# Unused but provided for reference. This refreshes the words within the array read for bitmap data. This was essential when loading bytes
					#	straight from the fileBuffer into the display as byte operations do NOT refresh the pixel thus a whole word operation must be performed.
		addi $sp, $sp, -12
		sw $t0, 8($sp)
		sw $t1, 4($sp)
		sw $t2, 0($sp)
		
#		lw $t0, totW
		li $t0, 0x04000		# constant in case totW is still broken
		la $t1, pxBuffer
	rfshLoop:
		lw $t2, ($t1)		# load word and store word to refresh the pixel, unfortunetely this is necessary
		sw $t2, ($t1)
		addi $t1, $t1, 4	# increment pointer by 1 word
		addi $t0, $t0, -1	# decrement counter
		bgtz $t0, rfshLoop	
								
		lw $t2, 0($sp)
		lw $t1, 4($sp)
		lw $t0, 8($sp)
		addi $sp, $sp, 12
		jr $ra
#######
	clearBuffer:			# unused but clears file buffer such that the memory space may be used for general purpose use. Again sort of irrelevant but kept for reference.
		addi $sp, $sp, -12
		sw $t0, 8($sp)
		sw $t1, 4($sp)
		sw $t2, 0($sp)
		
		lw $t0, buffLen		# load buffer length constant
		la $t1, fileBuffer
		li $t2, 0
	clearLoop:
		sw $t2, ($t1)		# store 0 in all buffer positions
		
		addi $t1, $t1, 4	# increment pointer
		addi $t0, $t0, -4	# decrement counter
		bgtz $t0, clearLoop	
								
		lw $t2, 0($sp)
		lw $t1, 4($sp)
		lw $t0, 8($sp)
		addi $sp, $sp, 12
		jr $ra
######################### System functions
	delay:			# requires $a0 as an argument in ms
		li $v0, 32	# system sleep
		syscall
		jr $ra
######################### Error Handler for File open failure
	openError:			# Called if $v0 = -1 after file open attempt ( -1 means open has failed )
		li $v0,	4
		la $a0,	errorOnRead
		syscall
	
		li $v0, 10		# program terminate if file fails to open
		syscall
