## This is just a collection of MIPS assembly files, intended to be used within the Mars 4.5 environment. ##
If issues occur with file reads either define your own path, or place the .asm and .bmp image in the same directory as Mars45.jar

Feel free to use this code for whatever but give some credit and maybe share what you do with it :)

All trig function calculators will not be updated, however in the FINAL.asm there are updated (faster) versions that solely work within registers instead of referencing .data values.

There is a known issue where 2 pixels in the slow, and 1 pixel in the fast BMP reader will be left as the previous value (updates delayed one frame).

There is a known issue where the slow BMP reader will flip colors on certain images but not others.

If animations are desired the image reader function may be called multiple times with differing $a0 pointer values.

Working .bmp files are provided in Downloads to the left, an image showing the GIMP export settings to create new images is also in there.

These programs should be well able to scale to any (fixed) image size with minor modification.