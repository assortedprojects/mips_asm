# If any part of this is useful feel free to use, but give some credit :)
# Much faster, but less pretty way to display BMP images ( 128 x 128, 32 bit X8R8G8B8 or A8R8G8B8 format, no colorspace header, uncompressed [no RLE])
.data
fileBuffer:		.space	0x10060

bmpFile1:	.asciiz	"subSonarBitmap.bmp"
bmpFile2:	.asciiz	"test.bmp"
bmpFile3:	.asciiz	"test2.bmp"
bmpFile4:	.asciiz	"test3.bmp"
bmpFile5:	.asciiz	"testFrac.bmp"
bmpFile6:	.asciiz	"wave.bmp"
bmpFile7:	.asciiz	"sunset.bmp"

.text
	main:
		la $a0, bmpFile1
		jal loadAndPrint
	
		li $a0, 750
		jal delay
	
		la $a0, bmpFile2
		jal loadAndPrint
		
		li $a0, 750
		jal delay
	
		la $a0, bmpFile4
		jal loadAndPrint
		
		li $a0, 750
		jal delay
	
		la $a0, bmpFile5
		jal loadAndPrint
		
		li $a0, 750
		jal delay
	
		la $a0, bmpFile6
		jal loadAndPrint
		
		li $a0, 750
		jal delay
	
		la $a0, bmpFile7
		jal loadAndPrint
		
		j term
	
	loadAndPrint:
		li $v0,	13		# read file
		
		li $a1,	0		# 0 flag
		li $a2, 0		# 0 flag
		syscall
		
		move $t1, $v0		# if file opens successfully save file descriptor in $t1
		
		li $v0, 14		# read from file
		addi $a0, $t1, 0	# load file descriptor into $a0
		la $a1, fileBuffer	# output buffer for file read
		addi $a1, $a1, -0x44	# offset for bmp file header header
		li $a2, 0x10054		# bytes to read ( px width * px height * 4 )
		syscall
		
		li $v0, 16		# close file
		move $a0, $t1		# load file descriptor into file, t1 can now be rewritten
		syscall
		
		#j term
		
		li $t4, 0x10000		# byte count of buffer
		la $t5, fileBuffer
		loopTop:
		#	lb $t0, ($t5)
		#	sb $t0, -1($t5)
			
			lb $t0, 0($t5)	# 0x000000XX
			lb $t1, 1($t5)	# 0x0000XX00
			lb $t2, 2($t5)	# 0x00XX0000
			lb $t3, 3($t5)	# 0xXX000000 
			#	Words are off by 1 byte and also flipped endianness
			#	
			# This code shifts the word -1 bytes, and re-orders the color bits into the 0x00RRGBB format we need
			sb $t0, -1($t5)		# shift byte to end of word, also corrects 1 byte offset from file read
			sb $t1, 0($t5)		# shift B to LSB
			sb $t2, 1($t5)		# shift G to middle
			sb $t3, 2($t5)		# shift R to MSB (of 24 bit colorspace)
			
			addi $t5, $t5, 4	# increment buffer pointer
		
			addi $t4, $t4, -4
			bgtz $t4, loopTop
		
		#j term
		
		li $t4, 0x2000			# word count of buffer / 2
		la $t5, fileBuffer		# base address of buffer
		la $t6, fileBuffer+0x10000	# last address of buffer
		
		loopTop2:			# load words, shift appropriately, then store at opposing corners of array ( across y = x )
			# to process the y = x flip we will fill from the top and bottom at the same time, filling towards the center row ( 63-64 ) boundary
			lw $t0, ($t5)		# load word from start of array
			lw $t1, ($t6)		# load word from end of array
			srl $t0, $t0, 8		# shift words appropriately to dispose of A8 or X8 colorspace info
			srl $t1, $t1, 8
			
			sw $t0, ($t6)		# store words across y = x
			sw $t1, ($t5)
			
			addi $t5, $t5, 4	# increment and decrement half pointers
			addi $t6, $t6, -4
			
			addi $t4, $t4, -1
			bgtz $t4, loopTop2
		
		
		li $t3, 128		# row counter
		la $t4, fileBuffer	
		
		loopTop3:			# Reverse row order, flipping image horizontally
			li $t5, 64		# Column counter / 2 ( words )
			addi $t6, $t4, 0	# start of row pointer reset
			la $t7, 0x200($t4)	# end of row pointer reset	
			loopTop4:			# Swap order of words within a row
					lw $t0, ($t6)
					lw $t1, ($t7)
					sw $t0, ($t7)
					sw $t1, ($t6)
					
					addi $t6, $t6, 4
					addi $t7, $t7, -4
					
					addi $t5, $t5, -1
					bnez $t5, loopTop4
					
			addi $t4, $t4, 0x200	# increment 1 row worth of addresses		
			addi $t3, $t3, -1	# decrement row counter
			bgtz $t3, loopTop3
		
		jr $ra		# return to main
		
		term:
		li $v0, 10
		syscall

######################### System functions
	delay:			# requires $a0 as an argument in ms
		li $v0, 32	# system sleep
		syscall
		jr $ra
